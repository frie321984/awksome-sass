#! /usr/bin/awk -f

# INBOX
# - mehrere selectoren in einer zeile mit komma getrennt
# - tests aus der SASS-Spec lesen und direkt hier testen obs schon geht? Bsp:
#    https://github.com/sass/sass-spec/blob/master/spec/core_functions/string/index.hrx
# - rechnen mit variablen
# x --version abfragen lassen
# x Kommandozeilenparams auslesen
# X Ich möchte tests mit debug flag starten können
# x Reihenfolge von CSS ist wichtig!
# X &.klasse
# X leere css selektoren vermeiden
# X mehrere variablen in einer zeile ersetzen (padding)
# X Kommentare im css-statement
# X Kommentare im "selector"
# x mehrere Kommentare speichern
# x makefile? http://www.schulz-koengen.de/biblio/makefiles.htm
# _ ~~Script aufteilen ums lesbarer zu machen~~ 
#     Mach ich nicht. wenn alles in einer datei ist kann ichs leichter runterladen.

BEGIN {
    RS="[{};]";
    FS=":";
    DEBUG=0;
	INDENTATION="  ";

	checkCommandLineOptions();
	dbg("Debug ist eingeschaltet.");

	# global variables
    currentSelector="";
	# comment[""][0]="";
	# content[""][0]="";
	# logOfSelectorsInCorrectOrder[0]="";
}
{
    dbg("-----------------------");
    dbg(NR ": " $0);
}
/\$[^:]+:/ {	
	dbg("Variable");
	parseAndStoreVariable($0);
	next;
}
/\/\*/ && RT!="{" {
	dbg("comment in body found");
	rememberSelectorOrder(currentSelector, logOfSelectorsInCorrectOrder);
	storeLineForSelector(currentSelector, NR, sanitizeComment($0), comment);
}
RT!="{" {
	dbg("css rules (body)");
	rememberSelectorOrder(currentSelector, logOfSelectorsInCorrectOrder);
	storeLineForSelector(currentSelector, NR, $0, content);
}
/\/\*/ && RT=="{" {
	dbg("comment near selector found");
	storedCommentToSaveLater = sanitizeComment($0);
}
RT=="{" {
    dbg("new record started. updating selector");
	
	appendSelector($0, selectorHist);
	currentSelector=calculateCurrentSelector(selectorHist);
	
    dbg("new selector: " currentSelector);
}
RT=="{" && storedCommentToSaveLater {
	dbg("there was a comment we wanted to save later. storing it now.");
	rememberSelectorOrder(currentSelector, logOfSelectorsInCorrectOrder);
	storeLineForSelector(currentSelector, NR, storedCommentToSaveLater, comment);
}
RT=="}" {
    dbg("record ended. remove last selector from current selector");
	recordEndedRemoveLastSelector(selectorHist);
	currentSelector=calculateCurrentSelector(selectorHist);
    dbg("new selector: " currentSelector);
}
END {
	# TODO refactoren... das macht sehr viel hier
	# * handle variables in content
	# * merge comment & content arrays
	# * print the whole stuff
	replaceAllVariables(variablen, content);

	for (selectorNr in logOfSelectorsInCorrectOrder) {
		selector = logOfSelectorsInCorrectOrder[selectorNr];

		dbg("selector: " selector);
		if (selector in content) {
			prettySelector=prettifySelector(selector);
			
			print prettySelector " {" ;
			delete body;
			for (lineNr in content[selector]) {
				body[lineNr]=INDENTATION prettifyContent(content[selector][lineNr]);
			}
			if (selector in comment) {
				for (lineNr in comment[selector]) {
					if(lineNr in body) {
						# zeile gibts schon (content)
						vorherigerInhalt = body[lineNr];
						body[lineNr]=INDENTATION comment[selector][lineNr] "\n" vorherigerInhalt;
					} else {
						body[lineNr]=INDENTATION comment[selector][lineNr];
					}
				}
			}
			for (lineNr in body) {
				print body[lineNr];
			}
			print "}";
		}
	}
}

function sanitizeComment(line) {
	kommentar=line;
	sub(/.*\/\*/, "/*", kommentar);
	sub(/\*\/.*/, "*/", kommentar);
	sub(/\/\*.*\*\//, "");
	return kommentar;
}
function prettifySelector(selector) {
	sub(/^ /, "", selector);
	
	if (selector ~ /[,~][^ ]/) {
        dbg("add space behind , ~ ");
        gsub(/,/, ", ", selector);
        gsub(/~/, "~ ", selector);
    }
	if (selector ~ /&/) {
		sub(/ ?&/, "", selector);
	} 

	return selector;
}
function prettifyContent(c) {
	sub(/[ \t\n]*$/, "", c);
	sub(/^[ \t\n]*/, "", c);
	if ( c !~ /;$/ && c !~ /\/\*/ ) {
		sub(/$/, ";", c);
	}
	return c;
}

function dbg(txt) {
    if (DEBUG) {
		gsub(/[^\\]\*/, "\\*", txt);
        print txt >> "/dev/stderr";
    }
}
function storeLineForSelector(selector, lineNr, lineContent, storage) {
	if (lineContent !~ /^[ \t\n]*$/){
		dbg("storing line for selector [" selector "] [" lineContent "]");
		storage[selector][sprintf("%010d", lineNr)]=lineContent;
	} else {
		dbg("not storing because content was empty");
	}
}
function rememberSelectorOrder(currentSelector, selectorHistory) {
	for (i in selectorHistory) {
		if (selectorHistory[i] == currentSelector)
			return;
	}
	
	selectorHistory[length(selectorHistory)] = currentSelector;
}

function replaceAllVariables(vars, content) {
	dbg("replacing variables");
	if (isarray(content)) {
		for (selector in content) {
			for (i in content[selector]) {
				dbg(i ": " content[selector][i]);
				updatedLine=variablenErsetzen(vars, content[selector][i]);
				content[selector][i]=updatedLine;
				dbg(i ": " content[selector][i]);
			}
		}
	}
}
function variablenErsetzen(variablen, css) {
	for (key in variablen) {
		value = variablen[key];
		dbg("key: " key);
		dbg("value: " value);
		sub("\\$"key, value, css);	
		dbg(css);
	}
	return css;
}
function calculateCurrentSelector(history) {
	dbg("*** selector is being updated");
	currentSelector = "";
	if (isarray(history)) {
		for (i in history) {
			sel = history[i];
			dbg("   - " sel);
			currentSelector = currentSelector " " sel;
		}
	}
	return currentSelector;
}

function appendSelector(selectorToAdd, history) {
	gsub(/^[ \n\t]*/, "", selectorToAdd);
	gsub(/[ \n\t]*$/, "", selectorToAdd);

	idx = 0;
	if (isarray(history)) {
		idx=length(history);
	}
	history[idx] = selectorToAdd;
	dbg("appendSelector " selectorToAdd);
}

function parseAndStoreVariable(line){
	variableName=line;
	sub(/.*\$/, "", variableName);
	sub(/:.*/, "", variableName);
	dbg("variableName: [" variableName "]");

	variableValue=line;
	sub(/.*: */, "", variableValue);
	sub(/;.*/, "", variableValue);
	dbg("variableValue: [" variableValue "]");
	
	variablen[variableName]=variableValue;
}

function checkCommandLineOptions() {
	for (i = 1; i < ARGC; i++) {
        if (ARGV[i] == "--hilfe") {
            print "Optionen: ";
			print "--hilfe	Diese Hilfe";
			print "--versn	Versionsnummer von meinsassparser";
			exit 0;
		}
        if (ARGV[i] == "--versn") {
            print "sassparser 0.0.1";
			exit 0;
		}
        else if (ARGV[i] == "--dbg")
            DEBUG=1;
        else if (ARGV[i] ~ /^-./) {
            e = sprintf("%s: Unbekannte Option %c",
                    ARGV[0], substr(ARGV[i], 2, 1));
            print e > "/dev/stderr";
        } else
            break;
        delete ARGV[i];
    }
}

function recordEndedRemoveLastSelector(history) {
	dbg("removedLastSelector " length(history));
	if (isarray(history)) {
		delete history[length(history)-1];
	}
	dbg("removedLastSelector " length(history));
}
