#!/bin/bash

testAusAndererDatei() {
	compileAndCompare 'a { a: a; }' 'a {
  a: a;
}' "$1"
	return $?
}


testSimpleHeading() {
	compileAndCompare 'h1 {
			color: blue;
		}' 'h1 {
  color: blue;
}' "$1"
	return $?
}

testFail() {
	compileAndCompare 'h1 {
			color: blue;
		}' 'h1 {
  color: red;
}' "$1"
	
	assertFailure $? 'Das hier hätte failen sollen, weil die beiden Dateien nicht den gleichen inhalt haben...'
	return $?
}

testCompareExistingFiles() {
	compileAndCompareFile ./order-is-important.expected ./order-is-important.sass "$1"

	return $?
}


testHeaderInBody() {
	compileAndCompare 'body { 
		h1 {
			color: blue;
		}
		}' 'body h1 {
  color: blue;
}' "$1"
	return $?
}

testTwoHeadersInBody() {
	compileAndCompare 'body { 
		h1 {
			color: blue;
		}
		h2 {
			color: yellow;
		}
		}' 'body h1 {
  color: blue;
}
body h2 {
  color: yellow;
}' "$1"
	return $?
}


testVerschachtelungstiefe3() {
	compileAndCompare 'body { 
		div {
			background-color: #123456;
			h2 { color: yellow;	}
		}
		}' 'body div {
  background-color: #123456;
}
body div h2 {
  color: yellow;
}' "$1"
	return $?
}

testVerschachtelungstiefeX() {
	compileAndCompare 'eins { 
	zwei {
		drei {
			vier {
				fuenf {
					sechs {
						sieben {
							acht {
								color: pink;
							}
						}
					}
				}
			}
		}
	}
}' 'eins zwei drei vier fuenf sechs sieben acht {
  color: pink;
}' "$1"
	return $?
}

testAllesInEinerZeile() {
	compileAndCompare 'body { span { img { border: 1px solid black; }}}' \
	  'body span img {
  border: 1px solid black;
}' "$1"
	return $?
}


testMehrereDeklarationen() {
	compileAndCompare 'someSelector {
			color: black;
			background-color: white;
		}' 'someSelector {
  color: black;
  background-color: white;
}' "$1"
	return $?
}

testSiblings() {
	compileAndCompare 'div { ~ a { a: 1 } }' 'div ~ a {
  a: 1;
}' "$1"
	return $?
}

testComments() {
	compileAndCompare 'a { /* ein kommentar der übernommen werden sollte */ a:1; }' 'a {
  /* ein kommentar der übernommen werden sollte */
  a:1;
}' "$1"
	return $?
}


testCommentBeforeSelector() {
	compileAndCompare 'a { 
		/*comment-vor-b*/ b{ b:1; }' 'a b {
  /*comment-vor-b*/
  b:1;
}' "$1"
	return $?
}

testMultipleComments() {
	compileAndCompare 'a { 
		/*comment-vor-b*/ b{ /*Kommentar in B*/ b:1;
		/* Kommentar C */ c: c; }' 'a b {
  /*comment-vor-b*/
  /*Kommentar in B*/
  b:1;
  /* Kommentar C */
  c: c;
}' "$1"
	return $?
}


testMultipleCommentsInBody() {
	compileAndCompare \
'a { 
		/*comment-vor-b*/ 
		b { /*Kommentar in B*/ b:1;
		c: c;
		/* Kommentar D */ d: d; }' \
'a b {
  /*comment-vor-b*/
  /*Kommentar in B*/
  b:1;
  c: c;
  /* Kommentar D */
  d: d;
}' "$1"
	return $?
}
