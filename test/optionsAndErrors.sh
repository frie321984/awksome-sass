#!/bin/bash

testMissingFile() {
	compileAndCompareFile ./order-is-important.expected ./order-is-important.blub "$1"

	assertError $? 'Das hier hätte failen sollen, weil es die Datei simpleheading.blub nicht gibt...'
	return $?
}

testVersion() {
	version=$(../sassparser.awk --versn)
	expectedVersion="sassparser 0.0.1" 
	if [ "$version" != "$expectedVersion" ]
	then
		echo "Version ist nicht wie erwartet.";
		echo "Erwartet: $expectedVersion"
		echo "War aber: $version"
		return "$TEST_FAILED";
	fi
	echo "$version";
	return "$TEST_OK";
}


testZeigeHilfe() {
	hilfeText=$(../sassparser.awk --hilfe)
	
	if [[ ! "$hilfeText" =~ "Optionen" ]]
	then
		echo "Hilfetext sollte alle Optionen enthalten";
		echo "die als Kommandozeilenparameter mitgegeben";
		echo "werden können.";
		return "$TEST_FAILED";
	fi
	return "$TEST_OK";
}


testUnbekannterParameter() {
	hilfeText=$(../sassparser.awk --foobar 2> /dev/stdout)
	if [[ ! "$hilfeText" =~ "Unbekannte Option" ]]
	then
		echo "Es sollte ein Text mit 'Unbekannter Option'";
		echo "erscheinen.";
		return "$TEST_FAILED";
	fi
	return "$TEST_OK";
}


testDebugFlag() {
	echo 'div { foo:bar; }' | ../sassparser.awk --dbg 1> testDbgFlag.tmp 2> testDebugFlag.tmp

	parseWithDebugFlag=$(head -n1 testDebugFlag.tmp);

	if [[ ! "$parseWithDebugFlag" == "Debug ist eingeschaltet." ]]
	then
		echo "Debugflag sollte aktiviert werden.";
		return "$TEST_FAILED";
	fi
	return "$TEST_OK";
}