#!/bin/bash

testVariableErsetzen() {
	compileAndCompare '$var: 5px;
img {
	border: $var solid black;
}' 'img {
  border: 5px solid black;
}' "$1"
	return $?
}

testMehrereVariablenInEinerZeileErsetzen() {
	compileAndCompare '$padTopBottom: 10px; $padLeftRight: 0;
img {
	padding: $padTopBottom $padLeftRight;
}' 'img {
  padding: 10px 0;
}' "$1"
	return $?
}
