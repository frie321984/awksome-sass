#!/bin/bash


testDivMitKlasse() {
	INPUT=\
'div.foo {
	color: irrelevant;
}';

	EXPECTATION=\
'div.foo {
  color: irrelevant;
}';

	compileAndCompare "$INPUT" "$EXPECTATION" "$1"
	return $?
}

testDivMitKlasseDurchVerschachtelung() {
	INPUT='div { &.a { a: 1 } }';

	EXPECTATION=\
'div.a {
  a: 1;
}';

	compileAndCompare "$INPUT" "$EXPECTATION" "$1"
	return $?
}


testSimpleCommaSeparatedClassesShouldStayTheSame() {
	INPUT=\
'span, p {
	foo: bar;
}';

	EXPECTATION=\
'span, p {
  foo: bar;
}';

	compileAndCompare "$INPUT" "$EXPECTATION" "$1"
	return $?
}


testCommaSeparatedClassesWithFollowingSelector() {
	INPUT=\
'span, p {
  foo: bar;
}
div {
  foo2: bar2;
}
';

	EXPECTATION=\
'span, p {
  foo: bar;
}
div {
  foo2: bar2;
}';

	compileAndCompare "$INPUT" "$EXPECTATION" "$1"
	return $?
}

testALotOfCommaSeparatedClassesWithFollowingSelector() {
	INPUT=\
'span, p, img, section, h1, h2, h3 {
  foo: bar;
}
div {
  foo2: bar2;
}
';

	EXPECTATION=\
'span, p, img, section, h1, h2, h3 {
  foo: bar;
}
div {
  foo2: bar2;
}';

	compileAndCompare "$INPUT" "$EXPECTATION" "$1"
	return $?
}

testCommaseparatedCombinedSelectors() {
	INPUT=\
'section h1, div h2 {
  foo: bar;
}
img {
  foo2: bar2;
}
';

	EXPECTATION=\
'section h1, div h2 {
  foo: bar;
}
img {
  foo2: bar2;
}';

	compileAndCompare "$INPUT" "$EXPECTATION" "$1"
	return $?
}


xtestFoo() {
	INPUT=\
'div { h1, h2, h3 { h: 123 } }';

	EXPECTATION=\
'div h1, div h2, div h3 {
  h: 123;
}';

	compileAndCompare "$INPUT" "$EXPECTATION" "$1"
	return $?
}
