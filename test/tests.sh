#!/bin/bash

# shellcheck disable=SC1091
source ./basicCSS.sh
source ./classes.sh
source ./variablen.sh
source ./optionsAndErrors.sh

TEST_OK=0
TEST_FAILED=1
TEST_ERROR=2

before() {
	echo ''
}

after() {
	echo 'after'
}

# ----------------------------------------------
main() {
	errorCount=0
	failCount=0
	total=0
	declare -F | grep test | cut -d\  -f3 > _testnames.tmp

	before

	while read -r line
	do
		echo "Running test [$line]"
		$line "$line"
		returnCode="$?"
		total=$((total + 1))
		case $returnCode in
			"$TEST_OK")
				echo 'ERFOLG.'
				echo ""
				;;
			"$TEST_FAILED")
				echo "FEHLER beim Test $line: $returnCode" 
				echo ""
				failCount=$((failCount + 1))
				;;
			*)
				echo "Schwerwiegender anderer Fehler." 
				echo ""
				errorCount=$((errorCount + 1))
				;;
		esac
	done < _testnames.tmp

	after


	exitCode=0
	echo '---------------'
	echo "ERROR / FAIL / TOTAL"
	echo "$errorCount / $failCount / $total"
	echo '---------------'
	if [ $failCount -gt 0 ] || [ "$errorCount" -gt 0 ]
	then
		echo "There were test failures or errors."
		exitCode=1
	else
		echo 'Alle Tests liefen erfolgreich durch.'
	fi

	exit $exitCode
}

compileAndCompare() {
	sassInput=$1
	expectedOutput=$2
	testname=$3
	
	echo "$sassInput" > "./$3-input.tmp"
	echo "$expectedOutput" > "./$3-expectation.tmp"

	compileAndCompareFile "./$3-expectation.tmp" "./$3-input.tmp" "$testname"
	return $?
}

assertFilesAreEqual() {
	if ! diff "$1" "$2"
	then
		echo "$3 failed."
		return $TEST_FAILED
	fi
}

assertFailure() {
	returnCode=$1
	explanation=$2
	if [ "$returnCode" -eq $TEST_FAILED ]
	then
		return $TEST_OK
	fi

	echo "$explanation"
	return $TEST_FAILED
}

assertError() {
	returnCode=$1
	explanation=$2
	if [ "$returnCode" -eq $TEST_ERROR ]
	then
		return $TEST_OK
	fi

	echo "$explanation"
	return $TEST_FAILED
}

error() {
	msg="$1"
	echo "$msg"
	return $TEST_ERROR
}


guardFileExists() {
	filename="$1"
	additionalText="File"
	if [ "$2" ]
	then
		additionalText="[$2]"
	fi

	if [ ! -f "$filename" ]
	then
		error "$additionalText not found or is not a file."
		return $TEST_ERROR
	fi
}

compileAndCompareFile() {
	testCount=$((testCount + 1))

	expectedFilename="$1"
	if ! guardFileExists "$expectedFilename" "$3: file with expected output"
	then
		return $TEST_ERROR
	fi

	fileToTest="$2"
	if ! guardFileExists "$fileToTest" "$3: file to test"
	then
		return $TEST_ERROR
	fi

	../sassparser.awk --dbg "$fileToTest" 1> "${fileToTest}.css.tmp"  2> "${fileToTest}.dbg.tmp"

	assertFilesAreEqual "$expectedFilename" "${fileToTest}.css.tmp" "$3"

	return $?
}

main
