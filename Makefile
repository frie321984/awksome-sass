default: test

test: ./test/tests.sh \
  ./test/basicCSS.sh \
  ./test/classes.sh \
  ./test/variablen.sh \
  ./test/optionsAndErrors.sh
	cd test && ./tests.sh
	rm test/*.tmp

install: awksome-sass

uninstall:
	rm ~/bin/awksome-sass

awksome-sass: sassparser.awk
	chmod u+x sassparser.awk
	ln -s ${CURDIR}/sassparser.awk ~/bin/awksome-sass

.PHONY: test